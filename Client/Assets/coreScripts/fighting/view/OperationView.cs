﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.coreScripts.common;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.fighting.model;
using strange.extensions.dispatcher.eventdispatcher.api;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Assets.coreScripts.fighting.view
{
    public class OperationView : EventView
    {
        [Inject]
        public FightingCommon common { get; set; }

        public float delayHR { get; set; }
        public float delayVH { get; set; }

        public bool hrOnceLock { get; set; }
        public bool vhOnceLock { get; set; }

        public void OnPress(GameObject o)
        {
            CustomOperationEventData data = new CustomOperationEventData
            {
                type = GameConfig.OperationEvent.NORMAL_TOUCH,
                str = o.name,
                operationEventType = GameConfig.OperationEvent.ONPRESS
            };

            common.SwitchDir(data);

            if (data.dir == GameConfig.Direction.LEFT || data.dir == GameConfig.Direction.RIGHT)
            {
                if (hrOnceLock)
                    return;
                hrOnceLock = true;
                RunActionDelay(delayHR, () => { dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data); });
            }
            else 
            {
                if (vhOnceLock)
                    return;
                vhOnceLock = true;
                RunActionDelay(delayVH, () => { dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data); });
            }
            //dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data);
        }
        public void OnRelease(GameObject o)
        {


            CustomOperationEventData data = new CustomOperationEventData
            {
                type = GameConfig.OperationEvent.NORMAL_TOUCH,
                str = o.name,
                operationEventType = GameConfig.OperationEvent.ONRELEASE
            };

            common.SwitchDir(data);

            if (data.dir == GameConfig.Direction.LEFT||data.dir == GameConfig.Direction.RIGHT)
            {
                if (!hrOnceLock)
                    return;

                RunActionDelay(0.1f, () => { hrOnceLock = false; dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data); });
            }
            else 
            {
                if (!vhOnceLock)
                    return;

                vhOnceLock = true;
                RunActionDelay(0.1f, () => { vhOnceLock = false; dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data); });
            }
     
           // dispatcher.Dispatch(GameConfig.OperationEvent.NORMAL_TOUCH, data);
        }

        public void RunActionDelay(float time, Action ac)
        {
            StartCoroutine(DelayRun(time, ac));
        }

        IEnumerator DelayRun(float time, Action ac)
        {
            yield return new WaitForSeconds(time);
            if (ac != null)
            {
                ac.Invoke();
            }
        }

        //public void Reset() 
        //{
        //    dispatcher.Dispatch(GameConfig.CoreEvent.GAME_RESTART);
        //}

    }
}
