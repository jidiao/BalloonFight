﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.coreScripts.common;
using Assets.coreScripts.main.model;
using strange.extensions.command.impl;

namespace Assets.coreScripts.fighting.common
{
    public class InitBaseDataCommand:EventCommand
    {
        public override void Execute()
        {
            UnityEngine.Debug.Log("InitBaseDataCommand.Execute()");
            injectionBinder.Bind<User>().CrossContext().ToSingleton();
            injectionBinder.Bind<DataBaseCommon>().CrossContext().ToSingleton();
            injectionBinder.Bind<IUnitAPI>().To<CustomAPI>().CrossContext().ToSingleton();
            injectionBinder.GetInstance<DataBaseCommon>().Init();
        }
    }
}
