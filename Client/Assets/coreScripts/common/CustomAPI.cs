﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.coreScripts.common
{
    public class CustomAPI : IUnitAPI
    {
        #region IUnitAPI Members

        public void CreatImage(UnityEngine.Sprite sprite, UnityEngine.Transform t)
        {
            GameObject go = new GameObject(sprite.name);
            go.layer = LayerMask.NameToLayer("UI");
            go.transform.parent = t;
            go.transform.localScale = Vector3.one;
            Image image = go.AddComponent<Image>();
            image.sprite = sprite;
            image.SetNativeSize();
        }

        public UnityEngine.Sprite LoadSprite(string spriteName)
        {
            return Resources.Load<GameObject>("Sprite/" + spriteName).GetComponent<SpriteRenderer>().sprite;

        }

        public List<T> LoadConfig<T>() where T : class
        {
            return JsonConvert.DeserializeObject<List<T>>(Resources.Load<TextAsset>(string.Format("Config/{0}", typeof(T).Name)).ToString()); 
        }

        #endregion

    }
}
