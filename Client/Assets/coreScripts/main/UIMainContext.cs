﻿using System;
using System.Collections.Generic;
using Assets.coreScripts.common;
using Assets.coreScripts.common.SequenceCommand;
using Assets.coreScripts.fighting.common;
using Assets.coreScripts.main.view;
using strange.extensions.context.api;
using strange.extensions.context.impl;

namespace Assets.coreScripts.main
{
    public class UIMainContext : MVCSContext
    {
        protected override void mapBindings()
        {
            mediationBinder.Bind<UILoadingView>().To<UILoadingMediator>();
            commandBinder.Bind(GameConfig.CoreEvent.LOADING_FIGHTING).To<UILoadSceneCommand>();

            if (this == Context.firstContext)
            {
                commandBinder.Bind(ContextEvent.START).To<StartAppCommand>().To<InitBaseDataCommand>().Once().InSequence();
            }
            else
            {
                //commandBinder.Bind(ContextEvent.START).To<StartAppCommand>().To<InitBaseDataCommand>().Once();
            }



        }
    }
}
